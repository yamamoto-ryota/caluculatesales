package jp.alhinc.yamamoto_ryota.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ
		 //コマンドライン引数が渡されているかチェック
        if(args.length != 1) {
        	System.out.println("予期せぬエラーが発生しました。");
        	return;
        }

        System.out.println("ここにあるファイルを開きます=>" + args[0]);

        BufferedReader br1 = null;
        try {
        	File file = new File(args[0], "branch.lst");
        	if(!file.exists()) {
        		System.out.println("支店定義ファイルが存在しません。");
        		return;
        	}

        	FileReader fr1 = new FileReader(file);
        	br1 = new BufferedReader(fr1);

        	String line;
        	while((line = br1.readLine()) != null) {
        		System.out.println(line);
        	}

        }catch(IOException e) {
        	System.out.println("予期せぬエラーが発生しました。");
        } finally {
        	if(br1 != null) {
        		try {
        			br1.close();
        		}catch(IOException e) {
        			System.out.println("予期せぬエラーが発生しました。");
        		}
        	}
        }
        //フィルタの作成
        FilenameFilter filter = new FilenameFilter() {
        	public boolean accept(File f, String str) {
        		//拡張子と8桁の指定
        		if(str.matches("[0-9]{8}.rcd")) {
        			return true;
        		}else {
        			return false;
        		}
        	}
        };
        //数字8桁かつ.rcdが含まれるフィルタの作成
        File[] files = new File(args[0]).listFiles(filter);

        //結果の出力

        for(int i=0; i<files.length; i++ ) {

        	try {
            	FileReader fr1 = new FileReader(files[i]);
            	br1 = new BufferedReader(fr1);

            	String line;
            	while((line = br1.readLine()) != null) {
            		System.out.println(line);
            	}
            } catch(IOException e) {
            	System.out.println("エラーが発生しました。");
            }finally {
            	if(br1 != null) {
            		try {
            			br1.close();
            		}catch(IOException e) {
            			System.out.println("予期せぬエラーが発生しました。");
            		}
            	}
            }
        }
        //リストへの追加
        List<File> rcdFiles = new ArrayList<>();
        for(int i=0; i<files.length; i++ ) {
        	rcdFiles.add(files[i]);
        }

        //連番チェック
        for(int i=0; i < rcdFiles.size() - 1; i++) {
        	int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
        	int latter = Integer.parseInt(rcdFiles.get(i+1).getName().substring(0,8));

        	if((latter - former) != 1) {
        		System.out.println("売り上げファイル名が連番になっていません。");
        	}
        }

        //マップに格納
        Map<String, String> branchNames = new HashMap<>();
        Map<String, Long> branchSales = new HashMap<>();
        //支店コード,支店名
        try {
        	File file = new File(args[0], "branch.lst");
        	FileReader fr1 = new FileReader(file);
        	br1 = new BufferedReader(fr1);

        	String line;
        	while((line = br1.readLine()) != null) {
        		String[] items = line.split(",");
        		//支店定義ファイルフォーマットチェック
        		if((items.length != 2) || (!items[0].matches("^[0-9]{3}$"))) {
        			System.out.println("支店定義ファイルのフォーマットが不正です。");
        			return;
        		}

        		branchNames.put(items[0], items[1]);
        		branchSales.put(items[0],0L);
        	}
        } catch(IOException e) {
        	System.out.println("予期せぬエラーが発生しました。");
        }finally {
        	if(br1 != null) {
        		try {
        			br1.close();
        		}catch(IOException e) {
        			System.out.println("予期せぬエラーが発生しました。");
        		}
        	}
        }

        //支店コード,売上額

        for(int i=0; i<files.length; i++ ) {

        	try {
        		ArrayList<String> salesList = new ArrayList<>() ;
            	FileReader fr1 = new FileReader(files[i]);
            	br1 = new BufferedReader(fr1);
            	//該当ファイル名
            	String fileName = files[i].getName();

            	String line = "";
            	while((line = br1.readLine()) != null) {
            	salesList.add(line);
            	}
            	//売り上げファイルが2行でなかった場合、エラーメッセージを表示
            	if(salesList.size() != 2) {
            	System.out.println("<" + fileName + ">のフォーマットが不正です");
            	return;
            	}

            	//2行目が数字かどうか
            	String money = (String)salesList.get(1);
            	if(!money.matches("^[0-9]+$")) {
            		System.out.println("予期せぬエラーが発生しました。");
            		return;
            	}
            	//支店コード
            	String branchCode = (String) salesList.get(0);
            	//売上金額と、branchSalesマップへの加算
            	long salesValue = Long.parseLong(money);
            	Long salesAmount = branchSales.get(branchCode) + salesValue;

            	branchSales.put(branchCode, salesAmount);

            	//売り上げファイルの支店コード=支店定義ファイルの支店コードかどうかチェック
            	if(!branchNames.containsKey(branchCode)) {
            	 System.out.println("<" + fileName + ">の支店コードが不正です。");
            	 return;
            	}

            	//合計金額が11桁以上であるかチェック
            	if(salesAmount >= 10000000000L) {
            		System.out.println("合計金額が10桁を超えました。");
            		return;
            	    }

            } catch(IOException e) {
            	System.out.println("予期せぬエラーが発生しました。");
            }finally {
            	if(br1 != null) {
            		try {
            			br1.close();
            		}catch(IOException e) {
            			System.out.println("予期せぬエラーが発生しました。");
            		}
            	}
            }
        }
        System.out.println(branchNames);
        System.out.println(branchSales);

        //支店売り上げファイルへの出力
        try {
        	File outputFile =new File(args[0],"branch.out");
            FileWriter fw = new FileWriter(outputFile);
            BufferedWriter bw = new BufferedWriter(fw);

            bw.write("【支店別集計ファイル】");
            bw.newLine();

            for(String key : branchNames.keySet()) {
            	bw.write(key + ","+branchNames.get(key) + "," + branchSales.get(key));
            	bw.newLine();
            }

            bw.close();
        } catch (IOException e) {
        	System.out.println("予期せぬエラーが発生しました。");
        	return;
        }
	}

}
